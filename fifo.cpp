#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <iostream>
#include <unistd.h>

#include <ncurses.h>
#include <pthread.h>

using namespace std;

class queue
{
	uint8_t rd_ptr = 0;
	uint8_t wr_ptr = 0;
	uint8_t data[255] = {0};

public:
	void write(int x);
	int read();
	void check();
	bool is_empty = true;	
};


void queue::write(int x)
{
        data[wr_ptr] = x;
        wr_ptr++;
	this->is_empty = false;
	if(wr_ptr == rd_ptr) this->is_empty = true;
};

void queue::check()
{
	if(rd_ptr == wr_ptr) this->is_empty = true;
}

int queue::read()
{
	int x;

        x = data[rd_ptr];
        rd_ptr++;

	return x;
};

// Global
queue myfifo;

void *fifo_test_write_thread(void *param)
{
	uint8_t *x = (uint8_t *)param;
	
	while(1)
	{
		myfifo.write(rand() % 254 + 1);
		usleep(rand() % 200000);
	};
}

void *fifo_test_read_thread(void *param)
{
	uint8_t tmp = 0;
	while(1)
	do 
	{
		myfifo.check();
		if(myfifo.is_empty) continue;
		tmp = myfifo.read();
		printf("READ: %d\r\n", tmp);
	} while(myfifo.is_empty != true);
}

int main(void)
{
	pthread_t fifo_read_tid;
	pthread_t fifo_write_tid;

	pthread_create(&fifo_read_tid, NULL, fifo_test_read_thread, NULL);
	pthread_detach(fifo_read_tid);

	pthread_create(&fifo_write_tid, NULL, fifo_test_write_thread, NULL);
	pthread_detach(fifo_write_tid);

	cout << "Press CTRL-C" << endl;
	while(1);


	return 0;
};
